FROM python:latest 

RUN pip install django

COPY . /pythonproject
#WORKDIR /pythonproject
EXPOSE 8000
RUN pip install numpy

ENTRYPOINT ["python","/pythonproject/manage.py","runserver","0.0.0.0:8000"]
